#include "control.h"
#include "application.h"

/**
 * @brief 总调度器
 * 
 */
void TaskProcess(void)
{
    uint8_t i;
    for (i = 0; i < TASKS_MAX; i++) // 逐个任务时间处理
    {
        if (TaskComps[i].Run == 1) // 时间不为0
        {
            TaskComps[i].TaskHook(); // 运行任务
            TaskComps[i].Run = 0;    // 标志清0
        }
    }
}
