#include <reg52.h>
#include "main.h"
#include "control.h"
#include "application.h"
#include "timer.h"

sbit led = P1 ^ 0;

/**
 * @brief 10ms init 11.0592MHz
 * 
 */
void timer1Init(void) 
{
	TMOD = 0x01;
	TH0 = 0xDC;
	TL0 = 0;
	ET0 = 1;
	TR0 = 1;
	EA = 1;
}

static int timer_cnt = 0;
/**
 * @brief timer1 interrupt callback
 * 
 */
void timer1Callback() interrupt 1 
{
	unsigned char i = 0;
	TH0 = 0XDC;
	TL0 = 0;
	timer_cnt++;
	//100ms
	if (timer_cnt >= 10)
	{
		timer_cnt = 0;
		led = ~led;
	}
	for (i = 0; i < TASKS_MAX; i++) // 逐个任务时间处理
	{
		if (TaskComps[i].Timer) // 时间不为0
		{
			TaskComps[i].Timer--;		 // 减去一个节拍
			if (TaskComps[i].Timer == 0) // 时间减完了
			{
				TaskComps[i].Timer = TaskComps[i].ItvTime; // 恢复计时器值，从新下一次
				TaskComps[i].Run = 1;					   // 任务可以运行
			}
		}
	}
}

/**
 * @brief 主函数
 * 
 */
void main(void)
{
	// init the timer1
	timer1Init();

	while (1)
	{
		//实时任务调度
		TaskProcess();
	}
}
