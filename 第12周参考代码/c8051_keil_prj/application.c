#include <reg52.h>
#include "control.h"
#include "application.h"

sbit led1 = P1 ^ 1;
sbit led2 = P1 ^ 2;
sbit led3 = P1 ^ 3;
sbit led4 = P1 ^ 4;

TASK_COMPONENTS TaskComps[] =
    {
        {0, 60, 10, TaskDisplayClock}, // 显示时钟
        {0, 20, 20, TaskKeySan},       // 按键扫描
        {0, 30, 90, TaskDispStatus},   // 显示工作状态
        {0, 30, 300, TaskDispStatus2}, // 显示工作状态
                                       // 这里添加你的任务
};

/**
 * @brief 显示线程
 * 
 */
void TaskDisplayClock(void)
{
    led1 = ~led1;
}

void TaskKeySan(void)
{
    led2 = ~led2;
}

void TaskDispStatus(void)
{
    led3 = ~led3;
}
void TaskDispStatus2(void)
{
    led4 = ~led4;
}
