#ifndef APPLICATION_H
#define APPLICATION_H
#include <stdint.h>
#include "control.h"

extern void TaskDisplayClock(void);
extern void TaskKeySan(void);
extern void TaskDispStatus(void);

// 任务清单
typedef enum _TASK_LIST
{
    TAST_DISP_CLOCK,            // 显示时钟
    TAST_KEY_SAN,             // 按键扫描
    TASK_DISP_WS,             // 工作状态显示
     // 这里添加你的任务。。。。
     TASKS_MAX                                           // 总的可供分配的定时任务数目
} TASK_LIST;


extern TASK_COMPONENTS TaskComps[];

#endif
