#include "control.h"
#include "application.h"
#include <QDebug>

TASK_COMPONENTS TaskComps[] =
{
    {0, 60, 60, TaskDisplayClock},            // 显示时钟
    {0, 20, 20, TaskKeySan},               // 按键扫描
    {0, 30, 30, TaskDispStatus},            // 显示工作状态
     // 这里添加你的任务
};

void TaskDisplayClock(void)
{
    qDebug("task1, TaskDisplayClock running!");
}

void TaskKeySan(void)
{
    qDebug("task2, TaskKeySan running!");
}

void TaskDispStatus(void)
{
    qDebug("task3, TaskDispStatus running!");
}



