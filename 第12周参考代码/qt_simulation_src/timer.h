#ifndef TIMER_H
#define TIMER_H

#include <QThread>
#include <QTime>
#include <QDebug>


class Timer : public QThread
{
    Q_OBJECT

public:
    //Q_DECL_OVERRIDE也就是c++的override
    void run() Q_DECL_OVERRIDE;
};

#endif
