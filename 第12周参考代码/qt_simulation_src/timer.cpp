#include "timer.h"
#include "control.h"
#include "application.h"

void Timer::run()
{
    while(true)
    {
        msleep(10);

        //qDebug("TimerThread running!456");


        for (uint8_t i = 0; i < TASKS_MAX; i++)          // 逐个任务时间处理
        {
            if (TaskComps[i].Timer)          // 时间不为0
            {
                TaskComps[i].Timer--;         // 减去一个节拍
                if (TaskComps[i].Timer == 0)       // 时间减完了
                {
                    //qDebug("i = %d", i);
                    TaskComps[i].Timer = TaskComps[i].ItvTime;       // 恢复计时器值，从新下一次
                    TaskComps[i].Run = 1;           // 任务可以运行
                }
            }
        }

    }
}
