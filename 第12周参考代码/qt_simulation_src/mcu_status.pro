QT -= gui
QT += network

CONFIG += c++11 console
CONFIG -= app_bundle

#开启如下语句,并rebuild工程后有cmd出现
#CONFIG += console

#开启如下语句,并rebuild工程后release模式时没有cmd,用于产品发布
#CONFIG += release

CONFIG (release, debug|release) {
DEFINES += QT_NO_DEBUG_OUTPUT
}

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        timer.cpp\
        control.cpp\
        application.cpp

HEADERS += \
        main.h\
        timer.h \
        control.h\
        application.h



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


