/**
* @file led_threads.c
* @brief tdoa等业务功能
* @details  
* @author laizx
* @date 2021年03月28日
* @version v1.0.0
* @par Copyright(c): www.imyfit.com
* @par History: v1.0.0: laizx, 2021年03月28日, 初始版本\n
****************************/

#include "pt.h"
#include "pt-sem.h"
#include "pt_threads.h"
#include "pt_timer.h"
#include "led_threads.h"

#include "nrf.h"
#include "nrf_log.h"

//-----------------------------protothreads variable start------------------------
struct pt pt_red_led_struct;
static struct pt_timer pt_red_led_delay_struct;

struct pt pt_blue_led_struct;
static struct pt_timer pt_blue_led_delay_struct;
//-----------------------------protothreads variable end--------------------------

//-----------------------------global variable start------------------------------

//-----------------------------global variable end--------------------------------

//-----------------------------private variable start-----------------------------
/** led协程开启/关闭信号,默认开启.生产测试场景需要关闭 */
static bool led_threads_signal = true;
//-----------------------------private variable end-------------------------------

//-----------------------------protothreads functions start-----------------------
/**
 * @brief 		红色led闪烁协程
 *              开机:慢闪3次
 *				关机:快闪3次
 *				电量低:3秒/次闪烁,提醒充电
 *				充电中:1秒/次闪烁
 *				充电完成:常亮
 * @param[in]  	协程结构体
 * @param[out] 	none
 * @retval 		int
 */
int ptRedLedThread(struct pt *pt)
{
	PT_BEGIN(pt);

	while (1)
	{
		/** 协程被允许执行 */		
		PT_WAIT_UNTIL(pt, led_threads_signal);

		/** 控制开关机的状态反转(瞬态量),方便LED提示 */
		static bool _mx_power_status = true;

		/** 开机状态(连续量),只有开机状态才允许电量低提示 */
		if (g_power_is_on)
		{
			/** 只处理一次开机事件(瞬态量) */
			if (_mx_power_status)
			{
				_mx_power_status = false;
				/** 开机：慢闪3次 */
				LED_OFF(LED_R);

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 400);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 400);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 400);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 400);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 400);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_OFF(LED_R);
				LED_PT_LOG("power on.");
			}
			/** 处理电量低提示,3秒/次闪烁,提醒充电 */
			if (g_wk_battery_charge_status == WK_BATTERY_IS_NEED_CHARGING)
			{
				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 3000);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));
				LED_PT_LOG("battery is low power.");
			}
		}
		/** 关机状态(连续量) */
		else
		{
			/** 只处理一次关机事件(瞬态量) */
			if (!_mx_power_status)
			{
				_mx_power_status = true;
				/** 关机：快闪3次 */
				LED_OFF(LED_R);

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 100);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 100);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 100);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 100);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 100);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));

				LED_OFF(LED_R);
				LED_PT_LOG("power off.");
			}
		}

		/** 无论是开机还是关机,均需处理:充电中/充电完成/没有充电 */
		{
			if (g_wk_battery_charge_status == WK_BATTERY_IS_CHARGING)
			{
				LED_TOGGLE(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 1000);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));
				//LED_PT_LOG("battery is charging.");
			}
			else if (g_wk_battery_charge_status == WK_BATTERY_CHARGE_COMPLETE)
			{
				LED_ON(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 1000);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));
				//LED_PT_LOG("battery charge is completed.");
			}
			else if (g_wk_battery_charge_status == WK_BATTERY_IS_NOT_CHARGING)
			{
				LED_OFF(LED_R);
				ptDelaySet(&pt_red_led_delay_struct, 1000);
				PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_red_led_delay_struct));
				//LED_PT_LOG("battery isn't charging.");
			}
		}

		PT_YIELD(pt);
	}
	PT_END(pt);
}

/**
 * @brief 		蓝色led闪烁协程,用于按键单击开关SOS
 *              国际莫尔斯码求救信号
 * @param[in]  	协程结构体
 * @param[out] 	none
 * @retval 		int
 */
int ptBlueLedThread(struct pt *pt)
{
	PT_BEGIN(pt);
	while (1)
	{
		/** 协程被允许执行 */		
		PT_WAIT_UNTIL(pt, led_threads_signal);

		/** 只有开机状态下才允许SOS */
		PT_WAIT_UNTIL(pt, g_power_is_on);

		/** 控制LED状态反转(瞬态量) */
		static bool _blue_led_status_is_on = false;

		/** 用户按了SOS按键,触发蓝灯发出SOS信号 */
		if (g_sos_is_on)
		{
			/** 只要进入SOS模式则认为LED是开启状态,即使是过程中处于关闭状态 */
			_blue_led_status_is_on = true;

			/**  三个快闪烁来表示"S" */
			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 150);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 150);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 150);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			/** 100毫秒延时,字母之间的间隙 */
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			/** 三个短闪烁来表示字母"O" */
			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 400);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 400);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 400);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			/** 100毫秒延时,字母之间的间隙 */
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			/** 再用三个快闪烁来表示字母"S" */
			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 150);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 150);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			LED_ON(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 150);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
			LED_OFF(LED_B);
			ptDelaySet(&pt_blue_led_delay_struct, 100);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}

			/** 等待5秒后重复S0S */
			ptDelaySet(&pt_blue_led_delay_struct, 5000);
			PT_WAIT_UNTIL(pt, !g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
			if (!g_sos_is_on)
			{
				LED_OFF(LED_B);
				PT_EXIT(pt);
			}
		}
		/** 用户取消SOS,蓝灯熄灭 */
		else
		{
			/** 判定LED状态,防止多次循环访问I/O浪费功耗 */
			if (_blue_led_status_is_on)
			{
				LED_OFF(LED_B);
				_blue_led_status_is_on = false;
				LED_PT_LOG("blue led turn off only once.");
			}
			ptDelaySet(&pt_blue_led_delay_struct, 2000);
			PT_WAIT_UNTIL(pt, g_sos_is_on || ptDelayExpired(&pt_blue_led_delay_struct));
		}

		PT_YIELD(pt);
	}
	PT_END(pt);
}

//-----------------------------protothreads functions end-------------------------

//-----------------------------others functions start-----------------------------
/**
 * @brief 开启协程等
 */
void ledRunAllThreads(void)
{
	led_threads_signal = true;
}

/**
 * @brief 关闭协程等
 */
void ledTerminateAllThreads(void)
{
	led_threads_signal = false;
}
//-----------------------------others functions end-------------------------------
