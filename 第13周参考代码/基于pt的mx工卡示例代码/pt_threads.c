/*******************************************************
filename:   	pt_threads.c
create:     	2019-02-07
revised: 			2019-02-25 laizhengxi;
revision:   	1.0;
description:  protothreads task
*******************************************************/
#include "pt.h"
#include "pt-sem.h"
#include "pt_threads.h"
#include "pt_timer.h"
#include "stdint.h"
#include "global_data_type.h"
#include <stdlib.h>
#include <math.h>

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "advertising.h"

//-----------------------------protothreads variable start------------------------
//
struct pt pt_demo_struct;
static struct pt_timer pt_demo_delay_struct;

/** 场景监听结构体 */
struct pt pt_scence_listen_struct;
static struct pt_timer pt_scence_listen_delay_struct;

/** 其它任务结构体 */
struct pt pt_other_routine_struct;
static struct pt_timer pt_other_routine_delay_struct;

//-----------------------------protothreads variable end--------------------------

//-----------------------------global variable start------------------------------

//-----------------------------global variable end--------------------------------

//-----------------------------private variable start-----------------------------

//-----------------------------private variable end-------------------------------

//-----------------------------protothreads functions start-----------------------
/**
 * @brief 		demo协程,可用作调试等
 * @param[in]  	协程结构体
 * @param[out] 	none
 * @retval 		int
 */
int ptDemoThread(struct pt *pt)
{
	PT_BEGIN(pt);
	while (1)
	{
		/** 延时1s左右 */
		ptDelaySet(&pt_demo_delay_struct, 1000 + PT_TIMER_TICK_STEP);
		PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_demo_delay_struct));

		//todo here
		//LED_TOGGLE(LED_R);
		//LED_TOGGLE(LED_B);

		static uint32_t _cnt = 0;
		_cnt++;
		if (connect_status_flag)
		{

			LED_TOGGLE(LED_B);
			//bleDebugMsgOutput("ble pt demo %d \r\n", _cnt);
		}
		else
		{
			if(!LED_STATUSE_RED(LED_B))
			{
				LED_OFF(LED_B);
			}
		}

		static uint8_t _tick = 0, _tick_cnt = 0;
		_tick++;
		if (_tick >= 10)
		{
			_tick = 0;
			PT_LOG("system tick %d.\r\n", _tick_cnt++);
		}

		PT_YIELD(pt);
	}
	PT_END(pt);
}

/**
 * @brief 		其它任务协程,例如电池电量adc
 * @param[in]  	协程结构体
 * @param[out] 	none
 * @retval 		int
 */
int ptOtherRoutineThread(struct pt *pt)
{
	PT_BEGIN(pt);
	while (1)
	{
		/** 延时1s */
		ptDelaySet(&pt_other_routine_delay_struct, 1000);
		PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_other_routine_delay_struct));

		/** 电池电量监测,10min更新一次电量 */
		static uint16_t _saadc_timer_count = 0;
		if (_saadc_timer_count++ > 600)
		{
			_saadc_timer_count = 0;
			wkSaadcStartAndWaitCompete();
			wkBatteryPercentValueUpdate();
			PT_LOG("battery = %d%%", wkBatteryPercentValueGet());
		}

		PT_YIELD(pt);
	}
	PT_END(pt);
}

/**
 * @brief 		终端场景实时监听协程.目前主要为室内场景和生产测试场景
 * @param[in]  	协程结构体
 * @param[out] 	none
 * @retval 		int
 */
int ptSceneListenThread(struct pt *pt)
{
	PT_BEGIN(pt);
	while (1)
	{

		/** 延时1s */
		ptDelaySet(&pt_scence_listen_delay_struct, 800);
		PT_WAIT_UNTIL(pt, ptDelayExpired(&pt_scence_listen_delay_struct));

		/** 场景没有发生变化,则直接退出 */
		if (sceneEventIsSet())
		{
			PT_LOG("scene changing!!");

			/** 停止所有协程 */
			tdoaTerminateAllThreads();
			ledTerminateAllThreads();
			/** 停止测试主协程 */
			testTerminateMainThreads();

			/** 进入不同的场景 */
			if (indoorSenceEventIsSet())
			{
				PT_LOG("indoor scene");
				g_current_scene = SCENE_UWB;
				tdoaRunAllThreads();
				ledRunAllThreads();
				indoorSenceEventClear();
			}
			else if (testSenceEventIsSet())
			{
				//todo here
				PT_LOG("test scene");
				g_current_scene = SCENE_TEST;
				/** 启动测试主协程,分项测试在协议解析中开启 */
				testRunMainThreads();
				testSenceEventClear();
			}
			else if (noneSenceEventIsSet())
			{
				//tod here

				noneSenceEventClear();
			}
		}

		PT_YIELD(pt);
	}
	PT_END(pt);
}

/**
 * @brief 		所有协程初始化,在main的初始化中调用
 * @param[in]  	none
 * @param[out] 	none
 * @retval 		none
 */
void ptInit(void)
{
	/** demo协程初始化 */
	PT_INIT(&pt_demo_struct);
	/** 其它任务协程初始化 */
	PT_INIT(&pt_other_routine_struct);
	/** 场景监听协程初始化 */
	PT_INIT(&pt_scence_listen_struct);
	/** tdoa协程初始化 */
	PT_INIT(&pt_tdoa_blink_struct);
	/** SOS LED协程初始化 */
	PT_INIT(&pt_blue_led_struct);
	/** 充电等状态LED协程初始化 */
	PT_INIT(&pt_red_led_struct);
	/** 测试:计步协程初始化 */
	PT_INIT(&pt_accel_struct);
	PT_INIT(&pt_accel_second_struct);

	/** 测试:监听协程初始化 */
	PT_INIT(&pt_test_listen_struct);
	/** 测试:蓝牙无线接收数据协程初始化 */
	PT_INIT(&pt_test_wrdp_struct);
	/** 测试:按键协程初始化 */
	PT_INIT(&pt_test_button_struct);
	/** 测试:充电协程初始化 */
	PT_INIT(&pt_test_charge_struct);
	/** 测试:led协程初始化 */
	PT_INIT(&pt_test_led_struct);
	/** 测试:空协程初始化 */
	PT_INIT(&pt_test_null_struct);
	/** 测试:uwb协程初始化 */
	PT_INIT(&pt_test_uwb_struct);
}

/**
 * @brief 		所有协程实时调度,在main的轮训中调用
 * @param[in]  	none
 * @param[out] 	none
 * @retval 		none
 */
void ptSchedule(void)
{
	/** demo协程调度 */
	ptDemoThread(&pt_demo_struct);
	/** 其它任务协程调度*/
	ptOtherRoutineThread(&pt_other_routine_struct);
	/** 场景监听协程调度 */
	ptSceneListenThread(&pt_scence_listen_struct);
	/** tdoa协程调度 */
	ptTdoaBlinkThread(&pt_tdoa_blink_struct);
	/** SOS LED协程调度 */
	ptBlueLedThread(&pt_blue_led_struct);
	/** 充电等LED协程调度 */
	ptRedLedThread(&pt_red_led_struct);
	/** 计步协程调度 */
	ptAccelThread(&pt_accel_struct);
	ptAccelSecondThread(&pt_accel_second_struct);

	/** 测试:监听协程调度 */
	ptTestListenThread(&pt_test_listen_struct);
	/** 测试:蓝牙无线接收数据协程调度 */
	ptTestWrdpThread(&pt_test_wrdp_struct);
	/** 测试:按键协程调度 */
	ptTestButtonThread(&pt_test_button_struct);
	/** 测试:充电协程调度 */
	ptTestChargeThread(&pt_test_charge_struct);
	/** 测试:led协程调度 */
	ptTestLedThread(&pt_test_led_struct);
	/** 测试:空协程调度 */
	ptTestNullThread(&pt_test_null_struct);
	/** 测试:uwb协程调度 */
	ptTestUwbThread(&pt_test_uwb_struct);
	/** 测试:accel协程调度 */
	ptTestAccelThread(&pt_test_accel_struct);
}

//-----------------------------protothreads functions end-------------------------

//-----------------------------others functions start-----------------------------

//-----------------------------others functions end-------------------------------
