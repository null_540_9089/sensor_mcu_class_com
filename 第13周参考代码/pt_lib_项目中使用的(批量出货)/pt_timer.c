#include <stdint.h>
#include "pt_timer.h"

//system tick variable, need used by hardware
volatile uint32_t pt_microsecond = 0;

/*******************************************************
@function:    		clock_time(void)
@description:  		get system tick
@param:
@return:
*******************************************************/
static int clock_time(void)
{
	return pt_microsecond;
}

/*******************************************************
@function:    		ptDelaySet(struct pt_timer *t, uint32_t interval)
@description:  		set protothread delay interval
@param:
@return:
*******************************************************/
void ptDelaySet(struct pt_timer *t, uint32_t interval)
{
	if (interval <= PT_TIMER_TICK_STEP)
	{
		t->interval = PT_TIMER_TICK_STEP;
	}
	else
	{
		t->interval = interval / PT_TIMER_TICK_STEP;
	}
	t->start = clock_time();
}

/*******************************************************
@function:    		ptDelayExpired(struct pt_timer *t)
@description:  		delay interval is arrival or not
@param:
@return:					return 1: arrival
*******************************************************/
uint32_t ptDelayExpired(struct pt_timer *t)
{
	uint32_t current_ms = clock_time();
	uint32_t start_ms = t->start;

	int32_t delta_ms = current_ms - start_ms;

	if (delta_ms <= 0)
	{
		t->interval -= (0xFFFFFFFF - (start_ms - 1));
		t->start = 0;
		return 0;
	}
	else
	{
		return (uint32_t)delta_ms >= (uint32_t)t->interval;
	}
}
