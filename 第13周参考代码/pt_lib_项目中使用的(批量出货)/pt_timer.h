#ifndef PT_TIMER_H
#define PT_TIMER_H

#include "stdint.h"

//system tick variable, need used by hardware
extern volatile uint32_t pt_microsecond;

//system tick step, default 10ms, need used by hardware
/** 功耗参见main.c内的说明 */
#define PT_TIMER_TICK_STEP 10

struct pt_timer
{
  uint32_t start;
  uint32_t interval;
};

/*******************************************************
@function:    		ptDelaySet(struct pt_timer *t, uint32_t interval)
@description:  		set protothread delay interval
@param:						
@return:
*******************************************************/
extern void ptDelaySet(struct pt_timer *t, uint32_t interval);

/*******************************************************
@function:    		ptDelayExpired(struct pt_timer *t)
@description:  		delay interval is arrival or not
@param:						
@return:					return 1: arrival
*******************************************************/
extern uint32_t ptDelayExpired(struct pt_timer *t);

#endif
